DESCRIPTION
-----------

This module allows you to have different taxonomy url alias and name.
It simple save last taxonomy url if checked on term edit form.
For example: you want to name your term "London" as "London 2013", 
and want to change it each year. If you enable checkbox, term url will be 
saved as "london", not as "london-2013". You may change taxonomy alias on
path module configuration page:

 Administer > Site building > URL aliases (admin/build/path)
  Search the last part of current alias (after forward slash).
 
INSTALLATION
------------

Standard module installation applies.

REQUIREMENTS
------------

Enabled taxonomy and pathauto modules.

CONFIGURATION
-------------

The only term item edit page is for settings.
